/*
	Repetition COntrol Structures (Loops)
		► Loops are one of the most important feature that a programming must have
		► It lets us execute a code repeatedly in a pre-set number or mayber forever.
	
	

		For Loop Syntax:

				for (Initialization; expression/condition; finalExpression) {
					statement;
				}

		While Loop Syntax:
			
			Initialization
			while (expression/condition){
				statement;
				finalExpression;
			}

		Do-While Loop Syntax:
			
			Initization
			do {
				statement;
				finaleExpression;
			} while (expression/condition)

	

*/

/*	
	Mini Activity

	 ► Create a function named greetinging and display a "Hello, my World!" using console.log inside the function.

	 ► Invoke the greetinging() function 20 times.

	 Solution:
	 	function greeting(){
	 		console.log("Hello, my World!")
	 	}
	 	greeting();greeting();greeting();greeting();greeting();greeting();greeting();greeting();greeting();greeting();greeting();greeting();greeting();greeting();greeting();greeting();greeting();greeting();greeting();greeting();

*/

function greeting(){
	 	console.log("Hello, my World!")
}

let count = 20;

while (count !== 0){
	console.log("This is printed inside the loop:"  + count)
	greeting();

	count--;
}
/*
	while Loop - takes in an expression/condition

	SYntax:

		declaring Variable (let/const/var varName)

		while(expression/condition) {
			statement

			count++/count--;
		}

*/

/*
	MiniActivity 2.
		► THe while

*/

let x = 1; // Initialization

while (x <= 5){ // Declaring the expression/condition
	console.log(x) // statement
	x++; // final expression
}

/*
	Do WHile Loop
		► a do-while loop works a lot like the while loop.
			But unlike while loops, do-while loops guarantees that the code will be executed at least once.
		► first its run the statement and the iterator then it will run the while loop (conditions)
	Syntax:
		do {
			statement

			var ++ / var --;


		} while (expression/condition)

*/

let number = Number(prompt("Give me a number")); // initalization

do {
	console.log("Do while: " + number); // statement
	number +=1; // finalExpression
} while (number <= 10) // Declaring the expression/condition


/*
	For Loops
		► more flexible than while & do-while loop

	Syntax:
		for (Initialization; expression/condition; finalExpression) {
			statement;
		}

*/

for (let count = 0; count<=20; count++){
	console.log("For Loop: "+ count);

}

// Can we use for loops in String?
let myString = "eric";

//Acessing element of individual element of String - use indexing10
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);
console.log(myString.length); // .length is to check on how many characters in the String


for (let x = 0; x < myString.length; x++) {
	console.log(myString[x]);

}

let myName = "alExandrO";

for (let i = 0; i < myName.length; i++){

	if( myName[i].toLowerCase() === "a" ||
		myName[i].toLowerCase() === "e" ||
		myName[i].toLowerCase() === "i" ||
		myName[i].toLowerCase() === "0" ||
		myName[i].toLowerCase() === "u") {
		console.log("Vowel!");
	} else {
		console.log(myName[i]);
	}

}

// Continue & Break Statements
/*

	"Continue" statemnet allows the code to go to the next iteration of the loop without finishing the execution of all the statements in a code block

	"Break" statement is used to terminate the current loop once the match has been found

*/

for (let count = 0; count <= 20; count++){
	if (count % 2 === 0){

		 console.log("Even Number");
		 continue;
	}

	console.log("Continue & break: " + count);

	if (count > 10) {
		break;
	}
}


let name = "alExandro";

for (let i=0; i<name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}
	if(name[i] === 'd'){
		break;
	}

}

